package main

import (
	"fmt"
	"math/rand"
	"strings"
)

func createPassword(s rand.Source, min, max, digits int) string {
	r := rand.New(s)
	var p string
	for len(p) < int(min) || len(p) > int(max) {
		p = ""
		p += adjectives[r.Intn(len(adjectives))] + "-"
		p += animals[r.Intn(len(animals))]
		for i, pw := 0, p+"-"; i < digits; i++ {
			pw += fmt.Sprintf("%v", r.Intn(9))
			p = pw
		}
		p = strings.ToLower(p)
		p = strings.Replace(p, " ", "-", -1)
	}
	return p
}
