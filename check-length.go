package main

var s, l = getShortest()

func checkLength() bool {
	if min > l {
		return false
	}
	if max < s {
		return false
	}
	return true
}

func getShortest() (int, int) {
	d := digits + 1
	if digits == 0 {
		d = 0
	}
	s := shortest(adjectives) + shortest(animals) + d
	l := longest(adjectives) + longest(animals) + d
	return s, l
}

func shortest(a []string) int {
	s := len(a[0])
	for i := 1; i < len(a); i++ {
		if len(a[i]) < s {
			s = len(a[i])
		}
	}
	return s
}

func longest(a []string) int {
	l := len(a[0])
	for i := 1; i < len(a); i++ {
		if len(a[i]) > l {
			l = len(a[i])
		}
	}
	return l
}
