# Password-gen

## Security

Password-gen is by no means a secure password generator. It uses a limited set of words in a predictable order and uses a software based random number generator.

## Usage

``` text
Usage:
  ./password-gen [options] count
  Where count is the number of passwords to generate (optional, default 1)

Options:
  -c    Shows the character count for each password
  -d int
        Number of digits to include at end of password(s) (default 3)
  -htpasswd string
        Add users to htpasswd file
  -max int
        Maximum amount of characters in generated password(s) (default 16)
  -min int
        Minimum amount of characters in generated password(s) (default 8)
  -seed int
        Seed to use for RNG (will use the the current nanosecond if set to 0)
  -u string
        Specify a username to prefix bofore the pasword
  -user-list string
        Specify a file containing a list of usernames seperated by commas.
```

## Instalation

Install golang from [golang.org](https://golang.org/dl/)

`go get github.com/Pwed/password-gen`