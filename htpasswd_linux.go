package main

import (
	"fmt"
	"os"
	"os/exec"
)

func htpasswd(user, password, file string) {
	err := exec.Command("htpasswd", "-b", file, user, password).Run()
	if err != nil {
		fmt.Println("Error from htpasswd command:", err)
		os.Exit(1)
	}
}
