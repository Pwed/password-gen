// +build !linux

package main

import (
	"fmt"
	"os"
)

func htpasswd(user, password, file string) {
	fmt.Println("htpasswd is only supported on linux")
	os.Exit(1)
}
