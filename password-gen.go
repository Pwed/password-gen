package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	min, max, digits                     int
	seed, count                          int64
	showCount                            bool
	username, userListFile, htpasswdFile string
	userList, args                       []string
)

func init() {
	flag.IntVar(&min, "min", 8, "Minimum amount of characters in generated password(s)")
	flag.IntVar(&max, "max", 16, "Maximum amount of characters in generated password(s)")
	flag.IntVar(&digits, "d", 3, "Number of digits to include at end of password(s)")
	flag.Int64Var(&seed, "seed", 0, "Seed to use for RNG (will use the the current nanosecond if set to 0)")
	flag.BoolVar(&showCount, "c", false, "Shows the character count for each password")
	flag.StringVar(&username, "u", "", "Specify a username to prefix bofore the pasword")
	flag.StringVar(&userListFile, "user-list", "", "Specify a file containing a list of usernames seperated by commas.")
	flag.StringVar(&htpasswdFile, "htpasswd", "", "Add users to htpasswd file (linux only at this point)")
	flag.Parse()
	args = flag.Args()
	flag.Usage = func() {
		fmt.Printf(usage, os.Args[0])
		flag.PrintDefaults()
	}
	count = int64(1)
}

func main() {
	if userListFile != "" {
		f, err := ioutil.ReadFile(userListFile)
		if err != nil {
			fmt.Println(err)
			flag.Usage()
			os.Exit(1)
		}
		userList = strings.Split(string(f), ",")
		count = int64(len(userList))
	}

	if len(args) == 1 {
		var e error
		if count, e = strconv.ParseInt(args[0], 10, 64); e != nil {
			fmt.Printf("Argument %v is not a number\nError message: %v\n", count, e)
			flag.Usage()
			os.Exit(2)
		}
	} else if len(args) != 0 {
		fmt.Printf("Too many arguments. Expected 0-1, recieved %v", len(args))
		flag.Usage()
		os.Exit(2)
	}

	if checkLength() && min <= max {
		for i := int64(0); i < count; i++ {
			if len(userList) != 0 {
				username = userList[i]
			}
			var s rand.Source
			if seed == 0 {
				s = rand.NewSource(time.Now().UnixNano() + int64(i))
			} else {
				s = rand.NewSource(seed + i)
			}
			p := createPassword(s, min, max, digits)
			if htpasswdFile != "" {
				if username == "" {
					fmt.Print("htpasswd can only be used with either the -u or -user-list flag set")
					os.Exit(1)
				}
				htpasswd(username, p, htpasswdFile)
			}
			if showCount {
				fmt.Printf("%s%*sLength: %v\n", p, max+4-len(p), "", len(p))
			} else if username != "" {
				fmt.Printf("%s:%s\n", username, p)
			} else {
				fmt.Println(p)
			}
		}
	} else {
		fmt.Println("Imposible constraints")
	}
}

const usage = `
Usage:
  %s [options] count
  Where count is the number of passwords to generate (optional, default 1)

Options:
`
